import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './New-Conference';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage"

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="attendees" element={<AttendeesList />} />
        <Route path="attendees/new" element={<AttendConferenceForm />} />
        <Route path="locations/new" element={<LocationForm />} />
        <Route path="conferences/new" element={<ConferenceForm />} />
        <Route path="presentations/new" element={<PresentationForm />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
