function createCard(name, description, picture_url, startDate, endDate, location) {
    return `
    <div class="card shadow h-100">
      <img src="${picture_url}" class="card-img-top h-flex" alt="...">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer text-muted">
        ${startDate}-${endDate}
      </div>
    </div>`
}


function formatDate(dateString) {
    const date = new Date(dateString);
    const options = { month: 'numeric', day: 'numeric', year: 'numeric' };
    return date.toLocaleDateString('en-US', options);
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            return alert(`${response.status}: ${response.url} ${response.statusText}\nFailed to fetch conferences`);
        } else {

            const data = await response.json();
            console.log(data);
            const container = document.getElementById("card-container");

            for (let conference of data.conferences) {
                console.log(conference)
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();

                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = formatDate(details.conference.starts);
                    const endDate = formatDate(details.conference.ends);
                    const location = details.conference.location.name;

                    const html = createCard(name, description, pictureUrl, startDate, endDate, location);
                    let div = document.createElement('div');
                    div.innerHTML = html;

                    container.appendChild(div);
                }
            }
        }

    }catch (e) {
        console.error(e.message);
    }
});
