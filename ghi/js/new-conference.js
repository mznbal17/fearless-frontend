window.addEventListener('DOMContentLoaded', async () => {
    const url = `http://localhost:8000/api/locations/`

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        const optionsContainer = document.querySelector('.form-select')

        for(const location of data.locations) {
            const href = location.href;
            const split = href.split("/");
            const strId = split[3];

            // const id = parseInt(strId);

            const option = document.createElement("option");
            option.value = strId;
            option.innerHTML = location.name;
            optionsContainer.appendChild(option);
        }

    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchOption = {
            method: "POST",
            body: json,
            headers: {
                "Content-Type": "application/json",
            },
        }
        const response = await fetch(conferenceUrl, fetchOption);
        if (response.ok) {
            formTag.reset();
        }
    })
})
